---
title: "Contribution"
weight: 2

---

# Contribution

Any contribution is highly appreciated.

If you intend to work on the source code of mathru, you should start by
forking the [repository](https://gitlab.com/rustmath/mathru).
Once you are done making modifications to your own copy, you should
create a pull request targeting the develop branch so that your
contribution can be reviewed, commented, and merged.

Clone the repo:
```
git clone https://gitlab.com/rustmath/mathru.git
```
Create a feature branch:
```
git checkout -b <feature_branch> master
```
After commiting your code:
```
git push -u origin <feature_branch>
```

Then submit a
[pull request](https://gitlab.com/rustmath/mathru/-/merge_requests),
preferably referencing the relevant
[issue](https://gitlab.com/rustmath/mathru/-/issues).

# Bug reporting
Please help by reporting any problems you find. Questions and feature
requests are best sent to the mailing list of the project it concerns.
Please report bugs, documentation errors etc., in the following tracker:

[Bug report](https://gitlab.com/rustmath/mathru/-/issues)
