---
title: "Polynomial"
weight: 1

---

# Polynomial
A polynomial is an expression that can be bilt from constants and symbols called variables or indeterminates by means of addition, multiplication and exponentiation to a non-negative integer power.

A polynomial in a single indeterminate $x$ can always be written (or rewritten) in the form
$$
a_n x^n + a_{n-1}x^{n-1} + \dotsb + a_2 x^2 + a_1 x + a_0
$$
where $a_{0},\ldots ,a_{n}$ are constants and $x$ is the indeterminate.

## Construction
Let's assume, that we want to construct the following polynomial:
$$
1 + 2x + 3x^2
$$
The construction of the same polynomial can be written in mathru with the following lines:
```rust
use mathru::algebra::abstr::Polynomial;

let a: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0, 3.0]);
```

## Arithmetic

### Addition

$$
  (1 + 2x + 3x^2) + (1 + 2x) = 2 + 4x + 3x^2
$$

```rust
let a: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0, 3.0]);
let b: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0]);

let c: Polynomial<f64> = Polynomial::new(vec![2.0, 4.0, 3.0]);

assert_eq!(c, &a + &b)
```

### Subtraction

$$
(1 + 2x + 3x^2) - (1 + 2x) = 3x^2
$$

```rust
let a: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0, 3.0]);
let b: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0]);

let c: Polynomial<f64> = Polynomial::new(vec![0.0, 0.0, 3.0]);

assert_eq!(c, &a - &b);
```

### Multiplication

$$
  (3x^2 + 2x + 1) (x + 1) = 3x^3 + 5x^2 + 3x + 1
$$

```rust
let a: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0, 3.0]);
let b: Polynomial<f64> = Polynomial::new(vec![1.0, 1.0]);

let c: Polynomial<f64> = Polynomial::new(vec![1.0, 3.0, 5.0, 3.0]);

assert_eq!(c, &a * &b)
```

### Division

$$
(3x^3 + 5x^2 + 3x + 1) / (3x^2 + 2x + 1) = (x + 1)
$$

```rust
use mathru::algebra::abstr::Polynomial;
use crate::mathru::algebra::abstr::Zero;

let a: Polynomial<f64> = Polynomial::new(vec![1.0, 2.0, 3.0]);
let b: Polynomial<f64> = Polynomial::new(vec![1.0, 1.0]);
let c: Polynomial<f64> = Polynomial::new(vec![1.0, 3.0, 5.0, 3.0]);

assert_eq!(b, (&c / &a).0);
assert_eq!(Polynomial::zero(), (&c / &a).1)
```