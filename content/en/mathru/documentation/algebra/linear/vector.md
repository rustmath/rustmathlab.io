---
title: "Vector"
weight: 1
---

# Vector

## Construction
Row and columns vectors with shapes known at run-time can be created
from the values of their components given in conventional mathematical
notation, i.e., row-by-rows, using a macro:

```rust
#[macro_use]
extern crate mathru;
use mathru::algebra::linear::vector::Vector;

fn main()
{
    // A row vector with three components
    let v_row: Vector<f64> = vector![1.0, 2.0, 3.0];

    // A column vector with three components
    let v_column: Vector<f64> = vector![1.0; 2.0; 3.0];
}
```

The comma separates the different components in a row and the semicolon separates the rows.