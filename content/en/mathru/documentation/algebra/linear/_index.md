---
title: "Linear Algebra"
weight: 1
---

# Linear Algebra

Vectors and matrices are the fundamental building blocks of any linear
algebra library. Their sizes are defined at run-time. They allow common
operations (additions, multiplications, etc.) to be used through
operator overloading.
