---
title: "Statistics"
date: 2020-07-06
weight: 1
---

# Statistics
This module contains a number of probability distributions as well as a growing library of statistical functions.
