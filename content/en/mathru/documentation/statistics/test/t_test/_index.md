---
title: "T-Test"
date: 2020-05-08
weight: 1
---


## One Sample two tailed test
For this test, suppose the data below. Moreover, suppose that the population variance is unknown
and that we would like to test whether the population mean is different from 0.

 $$
    X = ( 0.1, -0.4, 0.2, -0.4, 0.5 )
 $$

So we have:
* 5 observations: $n = 5$
* mean of the sample: $\bar{x} \approx 0.04 $


We want to test if the mean of the sampel is equal to zero or not.
We define our test hypothesis:

$$
    H_{0}: \mu_{0} = 0 
$$
and
$$
    H_{1}: \mu_{0} \neq 0
$$

Test statistics is defined as:
$$
    t = \sqrt n \frac{\bar{x} - \mu_{0}}{\sigma}
$$

With the given example, we get the following statistics
$$
    t = \sqrt 5 \frac{0.04 - 0.0}{0.416} \approx 0.215
$$

Critical value: $ \pm t(1.0 - \alpha/2, n-1) = \pm t() \approx \pm 0.225$.
The regions which are recjected are thus from $-\inf$ to -0.225 and from 0.225 to $+\inf$. The test
statistic is outside the rejection regionsd so we do not reject the
null hypothesis  $H_{0}$. In terms of the initial question: At the $\alpha = 0.05 $ significance
level, we do not reject the hypothesis that the population mean $\mu_{0}$ is equal to 0, or there
is no sufficient evidence in the data to conclude that the population mean is different from 0.

{{< include file="static/mathru/examples/t_test_one_sample_two_tailed.rs" language="rust" >}}

