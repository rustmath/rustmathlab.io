---
title: "Normal distribution"
date: 2020-05-01
weight: 2
---

# Normal distribution

## Probability density function
The probability density function for normal distribution is:
$$
    f(x) = \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{1}{2}(\frac{x-\mu}{\sigma})^2}
$$

![pdf normal distribution](/mathru/figures/pdf_normal_distribution.png "normal_distribution")

This plot can be generated with the following script:

{{< include file="static/mathru/examples/normal_distribution.rs" language="rust" >}}
