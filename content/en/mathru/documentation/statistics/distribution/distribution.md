---
title: "Distribution"
date: 2020-05-01
weight: 2
---

## Continuous trait

All continuous distribution implements the Continuous trait
{{< include file="static/mathru/src/statistics/distrib/distrib_impl.rs" language="rust" >}}

## Discrete trait

All discrete distribution implements the Discrete trait
{{< include file="static/mathru/src/statistics/distrib/distrib_impl.rs" language="rust" >}}
