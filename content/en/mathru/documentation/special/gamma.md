---
title: "Gamma functions"
date: 2020-08-16
weight: 2
---

# Gamma functions

## Gamma
The gamma function is defined as
$$
\Gamma(z) = \int_0^\infty t^{z-1} {\mathrm e}^{-t} \mathrm dt
$$
for $ z > 0.0 $.

### Example
Here is an example, how the gamma function can be used:

{{< include file="static/mathru/examples/gamma.rs" language="rust" >}}

This code snippet generates the following plot:

![Gamma function](/mathru/figures/gamma.png "Gamma function")

The vertical lines are caused by a bug in plotters(the used plotting library).

## Upper incomplete gamma function
The upper incomplete gamma function is defined as:
$$
\Gamma(s,x) = \int_x^{\infty} t^{s-1}\mathrm{e}^{-t}{\rm d}t
$$

### Example
```rust
use mathru::special::gamma;

let a: f64 = 0.5_f64;
let x: f64 = 0.3_f64;

let gamma_u: f64 = gamma::gamma_u(a, x);
```

## Lower incomplete gamma function
The lower incomplete gamma function is defined as:
$$
\gamma(s,x) = \int_0^x t^{s-1}\mathrm{e}^{-t}{\rm d}t
$$

### Example
```rust
use mathru::special::gamma;

let a: f64 = 0.5_f64;
let x: f64 = 0.3_f64;
let gamma_l: f64 = gamma::gamma_l(a, x);
```

## Upper regularized incomplete gamma function
The upper regularized incomplete gamma function is defined as:
$$
Q(s,x) = \frac{\Gamma(s,x)}{\Gamma(s)} = 1 - P(s,x)
$$

### Example
```rust
use mathru::special::gamma;

let a: f64 = 0.5_f64;
let x: f64 = 0.3_f64;
let gamma_ur: f64 = gamma::gamma_ur(a, x);
```

## Lower regularized incomplete gamma function
The lower regularized incomplete gamma function is defined as:
$$
P(s,x)=\frac{\gamma(s,x)}{\Gamma(s)} = 1 - Q(s, x)
$$

### Example
 ```rust
use mathru::special::gamma;

let a: f64 = 0.5_f64;
let x: f64 = 0.3_f64;
let gamma_lr: f64 = gamma::gamma_lr(a, x);
```