---
title: "Error functions"
date: 2020-07-06
weight: 1
---

# Error functions

## Error function
The error function (also called the Gauss error function), often denoted by erf, is a function defined as

$$
\operatorname{erf} z = \frac{2}{\sqrt\pi}\int_0^z e^{-t^2}dt
$$

### Example
{{< include file="static/mathru/examples/erf.rs" language="rust" >}}

![Error function](/mathru/figures/erf.png "Error function")

## Complementary error function

$$
\operatorname{erfc}(z) = 1 - \operatorname{erf} z,
$$