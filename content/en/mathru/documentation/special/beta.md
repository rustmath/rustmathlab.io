---
title: "Beta functions"
date: 2020-07-06
weight: 1
---

# Beta functions

## Beta function
In mathematics, the beta function, also called the Euler integral of the first kind, is a special function that is
 closely related to the gamma function and to binomial coefficients.  It is defined by the integral
$$
\Beta(x,y) = \int_0^1 t^{x-1}(1-t)^{y-1} dt
$$

## Incomplete beta function
The incomplete beta function, a generalization of the beta function, is defined as

$$
\Beta(x;a,b) = \int_0^x t^{a-1} (1-t)^{b-1} dt
$$

## Regularized incomplete beta function
The regularized incomplete beta function defined in terms of the incomplete beta function and the complete beta function:
$$
I_x(a,b) = \frac{\Beta(x;a,b)}{\Beta(a,b)}
$$
