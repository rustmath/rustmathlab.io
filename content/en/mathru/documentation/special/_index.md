---
title: "Special functions"
date: 2020-07-06
weight: 1
---

# Special functions

The main feature of this module is the definition of numerous special functions of mathematics.
Available functions include beta, gamma, hypergeometric and error.

