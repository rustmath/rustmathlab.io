---
title: "ODE"
weight: 1
---

# Ordinary Differential Equation

An ordinary differential equation(ODE) is an equation containing an unknown function of one real
 or complex variable $x$, its derivatives, and some given functions of $x$.
The term ordinary is used in contrast with the term partial differential equation, which may be
with respect to more than one independent variable.
The unknown function is generally represented by a variable (often denoted $y$), which depends on
$x$. Thus $x$ is often called the independent variable of the equation.

## General definition
Given $f$, a function of $x$, $y$, and derivatives of $y$. Then an equation of the form

$$ f ( x , y , y′ , \dots, y^{ (n − 1) } ) = y^{(n)} $$

is called an explicit ordinary differential equation of order $n$.

More generally, an implicit ordinary differential equation of order n takes the form:[10]

$$ f (x , y , y′ , y″ ,   … ,   y^{(n)} ) = 0 $$

## Reduction to a first-order system

Any explicit differential equation of order $n$,

$$ f ( x , y , y′ , \dots, y^{(n − 1)} ) = y^{(n)} $$

can be written as a system of $n$ first-order differential equations by defining a new family of
unknown functions

$$y_{i} = y^{( i − 1 )}$$

for $i = 1, 2, \dots, n$. The $n$-dimensional system of first-order coupled differential
equations is

$$
    \begin{aligned}
        y_{1} &=& y \\\\
        y_{2} &=& y_1'\\\\
        &\vdots&\\\\
        y_n &=& y_{(n-1)}'
    \end{aligned}
$$

$$
y^{n} = f(x, y_{1}, y_{2}, \dots, y_{n}) = y_{n}'
$$
This can be written as:
$$
    \begin{pmatrix}
        y_{1} \\\\
        y_{2} \\\\
        \vdots \\\\
        y_{n}
    \end{pmatrix}^{'}
    =
   \begin{pmatrix}
        y_{2} \\\\
        y_{3} \\\\
        \vdots \\\\
        f(x, y_{1}, y_{2}, \dots, y_{n})
    \end{pmatrix}
$$