---
title: "Differential Equation"
date: 2020-07-02
weight: 1
---

# Differential Equation

In mathematics, a differential equation is an equation that relates one or more functions and their derivatives.