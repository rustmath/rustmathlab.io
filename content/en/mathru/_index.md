---
title: "Mathru"
date: 2023-05-10
weight: 1
---

# Mathru
Mathru is a numeric library containing algorithms for linear algebra, analysis ,statistics and optimization written in pure Rust with optional BLAS/LAPACK as backend.

## Features
The following features are implemented in this create:
* [Algebra](https://rustmath.gitlab.io/mathru/documentation/algebra/)
    * [Abstract](https://rustmath.gitlab.io/mathru/documentation/algebra/abstract/)
        * [Polynomial](https://rustmath.gitlab.io/mathru/documentation/algebra/abstract/polynomial/)
            * Legendre polynomial
            * Chebyshev polynomial first & second kind
            * Bernstein polynomial
            * Bessel polynomial
    * [Linear algebra](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/)
        * [Vector](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/vector/)
        * [Matrix](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/)
            * Basic matrix operations(+,-,*)
            * Transposition (In-place)
            * [LU decomposition](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/#lu-with-partial-pivoting)
            * [QR decomposition](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/#qr)
            * [Hessenberg decomposition](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/#hessenberg)
            * [Cholesky decomposition](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/#cholesky)
            * Eigen decomposition
            * Singular value decomposition
            * Inverse
            * Pseudo inverse
            * Determinant
            * Trace
            * [Solve linear system](https://rustmath.gitlab.io/mathru/documentation/algebra/linear/matrix/#linear-system-resolution)

* Analysis
    * Integration
        * Newton-Cotes
        * Gauss-Legendre
    * [Ordinary differential equation (ODE)](https://rustmath.gitlab.io/mathru/documentation/analysis/differentialeq/)
        * [Explicit methods](https://rustmath.gitlab.io/mathru/documentation/analysis/differentialeq/ode/explicit/)
            * Euler method
            * Heun's 2nd order method
            * Midpoint method
            * Ralston's 2nd order method
            * Kutta 3rd order
            * Heun's 3rd order method
            * Ralston's 3rd order method
            * Runge-Kutta 4th order
            * Runge-Kutta-Felhberg
            * Dormand-Prince
            * Cash-Karp
            * Bogacki-Shampine
            * Adams-Bashforth
        * Automatic step size control with starting step size
        * [Implicit methods](https://rustmath.gitlab.io/mathru/documentation/analysis/differentialeq/ode/implicit)
            * Implicit Euler
            * Backward differentiation formula (BDF)
    * Interpolation
        * Cubic spline

* [Optimization](https://rustmath.gitlab.io/mathru/documentation/optimization)
    * Gauss-Newton algorithm
    * Gradient descent
    * Newton method
    * Levenberg-Marquardt algorithm
    * Conjugate gradient method

* [Statistics](https://rustmath.gitlab.io/mathru/documentation/statistics)
    * Probability distribution
        * Bernoulli
        * Beta
        * Binomial
        * Exponential
        * Gamma
        * Chi-squared
        * [Normal](https://rustmath.gitlab.io/mathru/documentation/statistics/distribution/normal/)
        * Log-Normal
        * Poisson
        * Raised cosine
        * Student-t
        * Uniform
    * [Test](https://rustmath.gitlab.io/mathru/documentation/statistics/test/)
        * Chi-squared
        * G
        * [Student-t](https://rustmath.gitlab.io/mathru/documentation/statistics/test/t_test/)

* Elementary functions
    * trigonometric functions
    * hyperbolic functions
    * exponential functions
    * power functions
    * trigonometric functions

* [Special functions](https://rustmath.gitlab.io/mathru/documentation/special)
    * [gamma functions](https://rustmath.gitlab.io/mathru/documentation/special/gamma/)
        * gamma function
        * log-gamma function
        * digamma function
        * upper incomplete gamma function
        * upper incomplete regularized gamma function
        * inverse upper incomplete regularized gamma function
        * lower incomplete gamma function
        * lower regularized incomplete gamma function
        * inverse lower regularized incomplete gamma function
    * [beta functions](https://rustmath.gitlab.io/mathru/documentation/special/beta/)
        * beta function
        * incomplete beta function
        * incomplete regularized beta function
    * [error functions](https://rustmath.gitlab.io/mathru/documentation/special/error/)
        * error function
        * complementary error function
        * inverse error function
        * inverse complementary error function
    * hypergeometric functions

## Reference Manual
The official API reference manual details functions, modules, and
objects etc included in mathru, describing what they are and what they
do. For learning how to use mathru, see also

[Reference manual](https://docs.rs/mathru/)
