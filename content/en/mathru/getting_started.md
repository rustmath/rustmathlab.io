---
title: "Getting Started"
date: 2020-07-19
weight: 1

---

# Introduction

Mathru is a collection of mathematical algorithms and convenience
functions implemented in Rust. This tutorial will acquaint the
first-time user Mathru with some of its most important features. It
assumes that the user has already some experience with Rust and Cargo.

## Getting started
Mathru relies on the official Rust package manager Cargo for dependency
resolution and compilation. Therefore, making Mathru ready to be used in
your project is simple. Add a new dependency to your Cargo.toml file.

```toml
[dependencies]
mathru = "^0.15.0"
```
Until Mathru reaches 1.0, it is highly recommended to use its latest
version, though you might encounter breaking changes from time to time.
Once your Cargo.toml file is set up, the corresponding crate must be
imported by your project with the usual extern crate directive. It is
recommended to use the mr abbreviation for referencing the crate
throughout your application.

```rust
use mathru as mr;
```

## Cargo example
You may use the Cargo.toml file to compile the downloadable examples of
this guide. Simply replace example.rs by the actual example’s file name.

```toml
[package]
name = "example-using-mathru"
version = "0.0.1"
authors = [ "Your name" ]

[dependencies.mathru]
version = "^0.15.0"

[[bin]]
name = "example"
path = "./example.rs"
```

## BLAS/LAPACK Support
Mathru provides bindings to BlAS/LAPACK. The operations supported by
BLAS/LAPACK are the same as those supported by the pure Rust version. As
a user, there is no difference between using the native Rust
implementation and BLAS/LAPACK.

BLAS/LAPACK support can be enable in the Cargo.toml file

```toml
[dependencies.mathru]
version = "^0.15.0"
default-features = false
features = "openblas"
```

One of the following implementations for linear algebra can be activated as a feature:
- native: Native Rust implementation(activated by default)
- [openblas](https://www.openblas.net): Optimized BLAS library
- [netlib](https://www.netlib.org): Collection of mathematical software, papers, and databases
- [intel-mkl](https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html): Intel Math Kernel Library
- [accelerate](https://developer.apple.com/documentation/accelerate) Make large-scale mathematical computations and image calculations, optimized for high performance and low-energy consumption.(macOS only)
