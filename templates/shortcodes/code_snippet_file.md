```{% if langsh -%}{{langsh}}{% endif -%}
{% if start_line -%}
{% else -%}
  {% set start_line = 0 -%}
{% endif -%}
{% set data_raw = load_data(path=path, format="plain") -%}
{% if end_line -%}
  {% set end_line = end_line + 1 -%}
{% else -%}
  {% set end_line = data_raw | length -%}
{% endif -%}
{% set data_lines = data_raw | split(pat="\n") -%}
{% set data_filtered = data_lines | slice(start=start_line, end=end_line) -%}
{% set index = 0 -%}
{% for s in data_filtered %}
{{ s | safe }}
{% endfor %}
```