
var theme = localStorage.getItem('theme');
var toggleToLightBtn = document.getElementById('toggleToLight');
var toggleToDarkBtn = document.getElementById('toggleToDark');

if (theme && theme === 'dark')
{
    toggleToLightBtn.className = 'navbar__icons--icon';
    toggleToDarkBtn.className = 'hide';
} else
{
    toggleToLightBtn.className = 'hide';
    toggleToDarkBtn.className = 'navbar__icons--icon';
}

// ======================= toggle theme =======================
var root = document.getElementById('root');
var toggleToLightBtn = document.getElementById('toggleToLight');
var toggleToDarkBtn = document.getElementById('toggleToDark');

toggleToDark.onclick = function(e)
{
    root.className = 'theme__dark';
    localStorage.setItem('theme', 'dark');
    toggleToLightBtn.className = 'navbar__icons--icon';
    toggleToDarkBtn.className = 'hide';
}

toggleToLight.onclick = function (e)
{
    root.className = 'theme__light';
    localStorage.setItem('theme', 'light');
    toggleToLightBtn.className = 'hide';
    toggleToDarkBtn.className = 'navbar__icons--icon';
}
